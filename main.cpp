#include <iostream>

#include "graph.h"

int main()
{
    Graph g;
    std::cin >> g;
    g.printSolution();

    return 0;
}
