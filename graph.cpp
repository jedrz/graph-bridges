#include <sstream>
#include <string>
#include <cmath>

#include "graph.h"

Graph::Vertex::Vertex(double x = 0, double y = 0)
    : x(x), y(y)
{
}

Graph::Edge::Edge(unsigned start, unsigned end, double cost)
    : start(start), end(end), cost(cost), bridge(false)
{
}

void Graph::addVertex(double x, double y)
{
    this->vertices.push_back(Graph::Vertex(x, y));
}

void Graph::addEdge(unsigned start, unsigned end, double cost)
{
    this->edges.push_back(Graph::Edge(start, end, cost));
}

// http://students.mimuw.edu.pl/~szreder/skrypt.pdf
// http://informatyka.wroc.pl/node/752
// http://was.zaa.mimuw.edu.pl/?q=node/42
void Graph::dfsLow(unsigned v, const Edges::iterator& parent)
{
    this->low[v] = this->entry[v] = ++entry_cnt;
    for (Edges::iterator edge = this->edges.begin();
         edge != this->edges.end();
         ++edge) {
        // Krawędź zawiera aktualnie rozpatrywany wierzchołek?
        if (edge->start == v || edge->end == v) {
            int u = edge->start == v ? edge->end : edge->start;
            // Upewnij się, że nie rozpatrujemy krawędzi do rodzica, ale
            // jedynie tej, którą weszliśmy do v. Możemy się wrócić, z powrotem
            // do rodzica, ale inną krawędzią.
            if (edge != parent) { // Porównuje adresy krawędzi (iteratory).
                if (this->entry[u]) {
                    // Wierzchołek na końcu krawędzi był już odwiedzony, czyli
                    // mamy krawędź niedrzewową.
                    this->low[v] = std::min(this->low[v], this->entry[u]);
                } else {
                    // Nie, puszczamy dfsa głębiej.
                    this->dfsLow(u, edge);
                    this->low[v] = std::min(this->low[v], this->low[u]);
                    // Krawędź jest mostem?
                    if (this->low[u] > this->entry[v]) {
                        edge->bridge = true;
                    }
                }
            }
        }
    }
}

void Graph::printSolution()
{
    // Przygotowanie do wywołania dfsa.
    this->entry_cnt = 0;        // Licznik czasu wejścia
    // Czas wejścia do danego wierzchołka.
    this->entry.assign(this->vertices.size(), 0);
    // Funkcja low.
    this->low.assign(this->vertices.size(), 0);

    // Zaczyna od wierzchołka o numerze 0.
    // Jeśli jakiś wierzchołek nie został odwiedzony oznacza to, że wejściowy
    // graf nie jest spójny. Wtedy należy szukać mostów w każdej ze składowych
    // (odpalać dfsa w jakimś, nieodwiedzonym wierzchołku składowej).
    for (unsigned next_v = 0; next_v < this->vertices.size(); ++next_v) {
        if (!this->entry[next_v]) {
            this->dfsLow(next_v, this->edges.end());
        }
    }

    // Wypisz wszystkie krawędzie oznaczone jako mosty.
    for (Edges::iterator edge = this->edges.begin();
         edge != this->edges.end();
         ++edge) {
        if (edge->bridge) {
            double d = std::sqrt(
                std::pow(
                    this->vertices[edge->start].x - \
                    this->vertices[edge->end].x, 2)
                + std::pow(
                    this->vertices[edge->start].y - \
                    this->vertices[edge->end].y, 2));
            std::cout << edge->start << " " << edge->end << " "
                      << edge->cost << " " << d << std::endl;
        }
    }
}

// Wyciąga dane z dziwnego wejścia.
std::istream& operator>>(std::istream& is, Graph& g)
{
    std::string line;
    while (getline(is, line)) {
        std::stringstream ss(line);
        double a, b, c;
        ss >> a >> b;
        if (!ss) {
            break;
        }
        if (ss.eof()) {         // Pierwsza sekcja
            g.addVertex(a, b);
        } else {                // Druga sekcja
            ss >> c;
            g.addEdge(a + 0.5, b + 0.5, c);
        }
    }
    return is;
}
