CC = g++
OBJ = main.o graph.o
SRC = main.cpp graph.cpp
BIN = graph
CFLAGS = -Wall -Wextra -pedantic

$(BIN): $(OBJ)
	$(CC) $(OBJ) -o $(BIN)

main.o: main.cpp
	$(CC) $(CFLAGS) -c main.cpp -o main.o

graph.o: graph.cpp
	$(CC) $(CFLAGS) -c graph.cpp -o graph.o

clean:
	rm -f *.o $(BIN)

check-syntax:
	$(CC) -fsyntax-only $(SRC)
