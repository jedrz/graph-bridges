#ifndef _GRAPH_H_
#define _GRAPH_H_

#include <iostream>
#include <vector>
#include <list>

class Graph
{
private:
    struct Vertex
    {
        double x, y;
        Vertex(double, double);
    };

    struct Edge
    {
        unsigned start, end;
        double cost;
        bool bridge;
        Edge(unsigned, unsigned, double);
    };

    typedef std::vector<Vertex> Vertices;
    typedef std::list<Edge> Edges;
    // Wektor wszystkich wierzchołków.
    // Indeks wektora oznacza numer wierzchołka.
    Vertices vertices;
    // Lista wszystkich krawędzi.
    Edges edges;
    std::vector<unsigned> entry, low;
    unsigned entry_cnt;
    void dfsLow(unsigned, const Edges::iterator&);
    void addVertex(double, double);
    void addEdge(unsigned, unsigned, double);
public:
    void printSolution();
    friend std::istream& operator>>(std::istream&, Graph&);
};

#endif
